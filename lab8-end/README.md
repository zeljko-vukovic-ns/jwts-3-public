﻿## Lab 8 - SPA, Backbone.js

* main.js:

```javascript
$(document).ready(function() {
	
	HomePageView = Backbone.View.extend({
		el : $('#container'), // "el" označava za koji deo HTML stranice je zadužen ovaj view, odnosno unutar čega će prikazivati
		template : _.template($('#homePageView').html()), // template je u templates.jsp unutar <script type="text/template" id="homePageView"> taga
		initialize : function() { // "initialize" je konstruktor za view
			this.render(); // iscrtaj stranicu
		},
		render : function() { // "render" je funkcija koja iscrtava view
			$(this.el).html(this.template()); // ubacivanje HTML-a definisanog u template-u u el element
		}
	});
	
	ActivitiesPageView = Backbone.View.extend({
		el : $('#container'), // "el" označava za koji deo HTML stranice je zadužen ovaj view, odnosno unutar čega će prikazivati
		template : _.template($('#activitiesPageView').html()), // template je u templates.jsp unutar <script type="text/template" id="activitiesPageView"> taga
		initialize : function() {
			this.render(); // iscrtaj stranicu
			this.showActivities(); // prikaži aktivnosti
		},
		render : function() { // "render" je funkcija koja iscrtava view
			$(this.el).html(this.template()); // ubacivanje HTML-a definisanog u template-u u el element
		},
		showActivities : function() { // funkcija za prikazivanje aktivnosti
			new ActivityCollectionView({ el : $('#activitiesTable tbody') }); // pravljenje ActivityCollectionView-a, i dinamičko setovanje "el" elementa kroz konstruktor
		}
	});
	
	ActivityModel = Backbone.Model.extend({
		urlRoot : 'api/activities', // "urlRoot" je korenska adresa na REST API (web servisu) za ovaj entitet
		defaults : { // podrazumevane vrednosti polja modela, Backbone model mora da se poklapa sa odgovarajućim DTO modelom
			id : null,
			name : null
		}
	});
	
	ActivityCollection = Backbone.Collection.extend({
		url : 'api/activities', // adresa na REST API (web servisu) gde se nalazi ova kolekcija
		model : ActivityModel // ovo je kolekcija Activity modela
	});
	
	ActivityCollectionView = Backbone.View.extend({
		model : new ActivityCollection, // model koji će prikazivan u view-u je ActivityCollection
		initialize : function() {
			var self = this; // kako bi ostala referenca na this unutar callback funkcije
			this.model.fetch({ // poziva se GET /api/activities
				success : function() { // callback ako je bilo 200 OK
					self.render();  // aktivnosti su uspešno dobavljene, preko self postoji referenca na this, pa se može pozvati render
				},
				error : function() { // callback ako nije bilo 200 OK
					alert('Error');
				}
			});
		},
		render : function() {
			for (id in this.model.models) { // models je kolekcija ActivityModel-a unutar ActivityCollection, u vidu mape (ključ je id, vrednost je objekat koji predstavlja ActivityModel)
				var activity = this.model.models[id]; // uzima se konkretna aktivnost (objekat ActivityModel) na osnovu id
				var activityItemView = new ActivityItemView({ model : activity }); // pravi se ActivityItemView, i dinamički mu se setuje model kroz konstruktor - model za ovaj view je konkretna aktivnost
				var activityItemViewDOM = activityItemView.render().el; // poziva se render za ActivityItemView, što izgeneriše HTML za taj red u tabeli, i .el vraća taj red kao DOM element
				$(this.el).append(activityItemViewDOM); // dodaje se red unutar #activitiesTable tbody (pogledati liniju 25)
			}
		}
	});
	
	ActivityItemView = Backbone.View.extend({
		tagName : "tr", // el nije eksplicitno specificiran, već je samo navedeno da će ovaj view biti unutar nekog tr taga
		template : _.template($('#activityItemView').html()), // template je u templates.jsp unutar <script type="text/template" id="activityItemView"> taga
		events : { // definisanje event-ova (događaja) u formatu "vrsta_eventa element" : "funkcija"
			"click .removeActivity" : "removeActivity"
		},
		render : function() {
			$(this.el).html(this.template({ activity : this.model.toJSON() })); // generisanje HTML preko template-a, ali uz dodatni podatak - activity, koji sadrži id i name aktivnosti
																				// this.model je zapravo aktivnost koja je postavljenja kroz parametar konstruktora (linija 58)
			return this; // vraća se this kako bi se moglo pristupiti "el" elementu ovog view-a (pogledati liniju 59)
		},
		removeActivity : function() {
			this.model.destroy(); // poziva DELETE /api/activities/{id} što obriše aktivnost na serverskoj strani
			$(this.el).remove(); // brisanje view-a, odnosno DOM elementa (el) koji ga predstavlja iz HTML-a
			return false; // return false zaustavlja propagaciju događaja klika
						  // po defaultu browser klikom na neki "a" tag ode na adresu definisanu u "href", a ovim se to sprečava i ostaje se na istoj stranici
		}
	});
	
	UsersPageView = Backbone.View.extend({
		el : $('#container'),
		template : _.template($('#usersPageView').html()),
		initialize : function() {
			this.render();
		},
		render : function() {
			$(this.el).html(this.template());
		}
	});
	
	AppRouter = Backbone.Router.extend({ // Backbone router je zapravo kontroler unutar klijentske MVC aplikacije (tj. ove Backbone aplikacije)
		routes : { // definisanje ruta u formatu "ruta" : "funkcija". "ruta" je u browseru zapravo "#ruta"
			"" : "homeRoute",
			"activities" : "activitiesRoute",
			"users" : "usersRoute"
		},
		homeRoute : function() {
			new HomePageView();
		},
		activitiesRoute : function() {
			new ActivitiesPageView();
		},
		usersRoute : function() {
			new UsersPageView();
		}
	});
	
	var appRouter = new AppRouter; // inicijalizacija routera da bi aplikacija radila, obavezno
	
	Backbone.history.start(); // omogućava privid navigacije (back, forward) kroz stranice preko hashtagova (#), iako je koris zapravo stalno na jednoj istoj stranici
});
```

* templates.jsp:

```html
<script type="text/template" id="homePageView">
<div class="container text-center">
    <!-- Example row of columns -->
    <h1>WAFEPA - Home</h1>
    <div class="row">
        <div class="col-md-6">
            <h2>Activities</h2>
            <p>CRUD operations on activities</p>
            <p><a class="btn btn-default" href="#activities">Go to activities</a>
            </p>
        </div>
        <div class="col-md-6">
            <h2>Users</h2>
            <p>CRUD operations on users</p>
            <p><a class="btn btn-default" href="#users">Go to users</a>
            </p>
        </div>
    </div>
    <hr>
</div>
</script>

<script type="text/template" id="activitiesPageView">
	<h1>Activities</h1>
	<table id="activitiesTable" class="table table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</script>

<script type="text/template" id="activityItemView">
	<!-- čitaju se atribut id i name iz objekta activity koji je prosleđen ovom template-u (pogledati liniju 72 u main.js) -->	
	<td><@= activity.id @></td>	
	<td><@= activity.name @></td>
	<td>
		<a class="removeActivity" href>Remove</a>
	</td>
</script>

<script type="text/template" id="usersPageView">
	<h1>Users</h1>
</script>
```