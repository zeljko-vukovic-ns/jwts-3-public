$(document).ready(function() {
	
	HomePageView = Backbone.View.extend({
		el : $('#container'),
		template : _.template($('#homePageView').html()),
		initialize : function() {
			this.render();
		},
		render : function() {
			$(this.el).html(this.template());
		}
	});
	
	ActivitiesPageView = Backbone.View.extend({
		el : $('#container'),
		template : _.template($('#activitiesPageView').html()),
		initialize : function() {
			this.render();
		},
		render : function() {
			$(this.el).html(this.template());
		}
	});
	
	UsersPageView = Backbone.View.extend({
		el : $('#container'),
		template : _.template($('#usersPageView').html()),
		initialize : function() {
			this.render();
		},
		render : function() {
			$(this.el).html(this.template());
		}
	});
	
	AppRouter = Backbone.Router.extend({
		routes : {
			"" : "homeRoute",
			"activities" : "activitiesRoute",
			"users" : "usersRoute"
		},
		homeRoute : function() {
			new HomePageView();
		},
		activitiesRoute : function() {
			new ActivitiesPageView();
		},
		usersRoute : function() {
			new UsersPageView();
		}
	});
	
	var appRouter = new AppRouter;
	
	Backbone.history.start();
});