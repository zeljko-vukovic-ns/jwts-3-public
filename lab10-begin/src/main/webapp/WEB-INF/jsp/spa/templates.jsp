﻿<script type="text/template" id="homePageView">
<div class="container text-center">
    <!-- Example row of columns -->
    <h1>WAFEPA - Home</h1>
    <div class="row">
        <div class="col-md-6">
            <h2>Activities</h2>
            <p>CRUD operations on activities</p>
            <p><a class="btn btn-default" href="#activities">Go to activities</a>
            </p>
        </div>
        <div class="col-md-6">
            <h2>Users</h2>
            <p>CRUD operations on users</p>
            <p><a class="btn btn-default" href="#users">Go to users</a>
            </p>
        </div>
    </div>
    <hr>
</div>
</script>

<script type="text/template" id="activitiesPageView">
	<h1>Activities</h1>
	<select id="searchField">
		<option value="none">----</option>
		<option value="name">Name</option>
		<option value="id">ID</option>
	</select>
	<input type="text" id="searchValue" />
	<button class="btn btn-default" id="searchActivities">Search</button>
	<button class="btn btn-default" id="clearSearchActivities">Clear</button>
	<br />
	<a href="#activities/add" class="btn btn-default">Add new activity</a>
	<table id="activitiesTable" class="table table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</script>

<script type="text/template" id="activityItemView">
	<!-- čitaju se atribut id i name iz objekta activity koji je prosleđen ovom template-u (pogledati liniju 72 u main.js) -->	
	<td><@= activity.id @></td>	
	<td><@= activity.name @></td>
	<td>
		<a class="removeActivity" href>Remove</a>
		<a class="editActivity" href="#activities/edit/<@= activity.id @>">Edit</a>
	</td>
</script>

<script type="text/template" id="usersPageView">
	<h1>Users</h1>
</script>

<script type="text/template" id="addEditActivityPageView">
<h1>WAFEPA - Add/edit activity</h1>
<div class="form-horizontal">

    <input type="hidden" id="activityId" />
    <div class="form-group">
        <label class="col-sm-2">Name </label>
        <div class="col-sm-6">
			<!-- upisuje se atribut name u text box iz objekta activity koji je prosleđen ovom template-u (pogledati liniju 110 u main.js) -->
            <input type="text" class="form-control" id="activityName" value="<@= activity.name @>" />
        </div>
		<div class="col-sm-4">
			<span class="label label-danger" id="name-error"></span>
		</div>
    </div>      

    <div class="form-group">
    	<div class="col-sm-offset-2 col-sm-10">
      		<button type="submit" class="btn btn-default" id="saveActivity">Save</button>
    	</div>
  	</div>
</div>
</script>