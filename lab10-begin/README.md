﻿## Lab 10 - Backbone.js - validation, i18n

### I18N

I18N (internationalization) predstavlja dizajniranje i razvoj proizvoda,
aplikacije ili bilo kakvog sadržaja tako da bude obezbeđena laka lokalizacija
za različite ciljne grupe koje variraju u kulturi, državi ili jeziku.

Spring Framework ima direktnu podršku za i18n, što omogućava razvoj web aplikacije
tako da lako bude dostupna na svim podržanim jezicima.