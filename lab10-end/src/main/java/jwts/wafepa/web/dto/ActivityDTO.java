package jwts.wafepa.web.dto;

import javax.validation.constraints.NotNull;

import jwts.wafepa.model.Activity;

import org.hibernate.validator.constraints.NotEmpty;

public class ActivityDTO {
	
	@NotNull
	@NotEmpty
	private String name;
	
	private Long id;
	
	public ActivityDTO() {
		super();
	}

	public ActivityDTO(Activity activity){
		super();
		
		this.id = activity.getId();
		this.name = activity.getName();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
