﻿$(document).ready(function() {
	
	var locale = localStorage.getItem('locale'); // dobavljanje vrednosti za locale
	if (!locale) { // ako locale ne postoji u localStorage (korisnik prvi put pristupa aplikaciji)
		locale = 'en-US'; // default locale je en-US
		localStorage.setItem('locale', locale); // snimi u localStorage
	}
	// definisanje koja će skripta sa lokalizovanim stringovima učitati
	var localeScript = '<script text="text/javascript" src="resources/js/i18n/' + locale + '.js"></script>';
	$('head').append(localeScript); // učitavanje skripte - dodavanje u head
	
	LocaleChangerView = Backbone.View.extend({ // view koji se koristi za promenu lokala
		el : $('#locale-changer'), // "el" označava za koji deo HTML stranice je zadužen ovaj view, odnosno unutar čega će prikazivati/ 
		events : { // definisanje event-ova (događaja) u formatu "vrsta_eventa element" : "funkcija"
			"click a#en-US" : "changeLocaleEnUS",
			"click a#sr-RS" : "changeLocaleSrRS"
		},
		changeLocaleEnUS : function() { // funkcija za promenu lokala na en-US
			localStorage.setItem('locale', 'en-US'); // snimi promenu locala u localStorage
			location.reload(true); // reload stranice
			return false; // return false zaustavlja propagaciju događaja klika
			  			  // po defaultu browser klikom na neki "a" tag ode na adresu definisanu u "href", a ovim se to sprečava i ostaje se na istoj stranici
		},
		changeLocaleSrRS : function() { // funkcija za promenu lokala na sr-RS
			localStorage.setItem('locale', 'sr-RS'); // snimi promenu locala u localStorage
			location.reload(true); // reload stranice
			return false; // return false zaustavlja propagaciju događaja klika
			 			  // po defaultu browser klikom na neki "a" tag ode na adresu definisanu u "href", a ovim se to sprečava i ostaje se na istoj stranici
		}
	});
	var localeChangerView = new LocaleChangerView();
	
	HomePageView = Backbone.View.extend({
		el : $('#container'), // "el" označava za koji deo HTML stranice je zadužen ovaj view, odnosno unutar čega će prikazivati
		template : _.template($('#homePageView').html()), // template je u templates.jsp unutar <script type="text/template" id="homePageView"> taga
		initialize : function() { // "initialize" je konstruktor za view
			this.render(); // iscrtaj stranicu
		},
		render : function() { // "render" je funkcija koja iscrtava view
			$(this.el).html(this.template()); // ubacivanje HTML-a definisanog u template-u u el element
		}
	});
	
	ActivitiesPageView = Backbone.View.extend({
		el : $('#container'), // "el" označava za koji deo HTML stranice je zadužen ovaj view, odnosno unutar čega će prikazivati
		template : _.template($('#activitiesPageView').html()), // template je u templates.jsp unutar <script type="text/template" id="activitiesPageView"> taga
		events : {
			"click #searchActivities" : "searchActivities", // event kada se klikne na dugme "Search"
			"click #clearSearchActivities" : "clearSearchActivities" // event kada se klikne na dugme "Clear"
		},
		initialize : function() {
			this.render(); // iscrtaj stranicu
			this.showActivities(); // prikaži aktivnosti
		},
		render : function() { // "render" je funkcija koja iscrtava view
			$(this.el).html(this.template()); // ubacivanje HTML-a definisanog u template-u u el element
		},
		showActivities : function(field, value) { // funkcija za prikazivanje aktivnosti
			new ActivityCollectionView({ el : $('#activitiesTable tbody'), field : field, value : value }); // pravljenje ActivityCollectionView-a, i dinamičko setovanje "el" elementa kroz konstruktor
		},
		searchActivities : function() { // funkcija za pretraživanje aktivnosti po zadatom kriterijumu
			var field = $('#searchField').val(); // po kom polju se pretražuje
			var value = $('#searchValue').val(); // vrednost polja po kom se pretražuje
			if (field != "none") {
				this.showActivities(field, value); // ako je izabrano neko polje, pretraži aktivnosti po zadatom kriterujumu
			} else {
				this.showActivities(); // u suprotnom prikaži sve aktivnosti
			}
		},
		clearSearchActivities : function() { // funkcija koja postavlja podrazumevane vrednost combobox-a i textbox-a za pretraživanje
			$('#searchField').val('none');
			$('#searchValue').val('');
			this.showActivities(); // prikazivanje svih aktivnosti
		}
	});
	
	ActivityModel = Backbone.Model.extend({
		urlRoot : 'api/activities', // "urlRoot" je korenska adresa na REST API (web servisu) za ovaj entitet
		defaults : { // podrazumevane vrednosti polja modela, Backbone model mora da se poklapa sa odgovarajućim DTO modelom
			id : null,
			name : null
		},
		validation : { // definisanje validacije za model
			name : { // za polje "name"
				required : true, // zahteva se, tj. ne sme biti prazno (pogledati liniju 136 šta se desi kada validacija ne prođe),
				msg : polyglot.t("nameInvalid") // poruka greške se čita iz lokalizovanih stringova po ključu "nameInvalid"
			}
		}
	});
	
	ActivityCollection = Backbone.Collection.extend({
		url : 'api/activities', // adresa na REST API (web servisu) gde se nalazi ova kolekcija
		model : ActivityModel // ovo je kolekcija Activity modela
	});
	
	ActivityCollectionView = Backbone.View.extend({
		model : new ActivityCollection, // model koji će prikazivan u view-u je ActivityCollection
		initialize : function(params) {
			var field = params.field;
			var value = params.value;
			var self = this; // kako bi ostala referenca na this unutar callback funkcije
			this.model.fetch({ // poziva se GET /api/activities
				data : $.param({ 'field' : field, 'value' : value }),
				success : function() { // callback ako je bilo 200 OK
					self.render();  // aktivnosti su uspešno dobavljene, preko self postoji referenca na this, pa se može pozvati render
				},
				error : function() { // callback ako nije bilo 200 OK
					alert('Error');
				}
			});
		},
		render : function() {
			$(this.el).html('');
			for (id in this.model.models) { // models je kolekcija ActivityModel-a unutar ActivityCollection, u vidu mape (ključ je id, vrednost je objekat koji predstavlja ActivityModel)
				var activity = this.model.models[id]; // uzima se konkretna aktivnost (objekat ActivityModel) na osnovu id
				var activityItemView = new ActivityItemView({ model : activity }); // pravi se ActivityItemView, i dinamički mu se setuje model kroz konstruktor - model za ovaj view je konkretna aktivnost
				var activityItemViewDOM = activityItemView.render().el; // poziva se render za ActivityItemView, što izgeneriše HTML za taj red u tabeli, i .el vraća taj red kao DOM element
				$(this.el).append(activityItemViewDOM); // dodaje se red unutar #activitiesTable tbody (pogledati liniju 25)
			}
		}
	});
	
	ActivityItemView = Backbone.View.extend({
		tagName : "tr", // el nije eksplicitno specificiran, već je samo navedeno da će ovaj view biti unutar nekog tr taga
		template : _.template($('#activityItemView').html()), // template je u templates.jsp unutar <script type="text/template" id="activityItemView"> taga
		events : { // definisanje event-ova (događaja) u formatu "vrsta_eventa element" : "funkcija"
			"click .removeActivity" : "removeActivity"
		},
		render : function() {
			$(this.el).html(this.template({ activity : this.model.toJSON() })); // generisanje HTML preko template-a, ali uz dodatni podatak - activity, koji sadrži id i name aktivnosti
																				// this.model je zapravo aktivnost koja je postavljenja kroz parametar konstruktora (linija 58)
			return this; // vraća se this kako bi se moglo pristupiti "el" elementu ovog view-a (pogledati liniju 59)
		},
		removeActivity : function() {
			this.model.destroy(); // poziva DELETE /api/activities/{id} što obriše aktivnost na serverskoj strani
			$(this.el).remove(); // brisanje view-a, odnosno DOM elementa (el) koji ga predstavlja iz HTML-a
			return false; // return false zaustavlja propagaciju događaja klika
						  // po defaultu browser klikom na neki "a" tag ode na adresu definisanu u "href", a ovim se to sprečava i ostaje se na istoj stranici
		}
	});
	
	AddEditActivityPageView = Backbone.View.extend({
		el : $('#container'), // "el" označava za koji deo HTML stranice je zadužen ovaj view, odnosno unutar čega će prikazivati
		model : new ActivityModel, // model za ovaj view je "ActivityModel", odnosno aktivnost koja se dodaje/menja
		template : _.template($('#addEditActivityPageView').html()), // template je u templates.jsp unutar <script type="text/template" id="addEditActivityPageView"> taga
		events : { // definisanje event-ova (događaja) u formatu "vrsta_eventa element" : "funkcija"
			"click #saveActivity" : "saveActivity" // event za snimanje aktivnosti
		},
		initialize : function(params) {
			if (params && params.id) { // edit - ako su prosleđeni parameteri u konstruktoru (pogledati liniju 164)
				var id = params.id;
				this.model.set("id", id); // modelu se postavlja id kako bi mogao da se uradi fetch
				var self = this;
				this.model.fetch({ // poziva se /api/activities/{id}
					success : function() { // ako je bilo 200 OK
						self.render();
					},
					error : function() { // ako nije bilo 200 OK
						alert('Error');
					}
				});
			} else { // add - ako nisu prosleđeni parametri u konstruktoru
				this.model = new ActivityModel;
				this.render();
			}
			
			// povezivanje validacije modela ActivityModel sa ovim view-om
			Backbone.Validation.bind(this, {
				invalid : function(view, field, error) { // funkcija koja se pozove kada validacija nije prošla
														 // parametri funkcije: "view" - unutar kog view-a validacija nije prošla, "field" - za koje polje, "error" - poruka greške
					$('#' + field + '-error').html(error);
				}
			});
		},
		render : function() {
			$(this.el).html(this.template({ activity : this.model.toJSON() })); // iscrtavanje stranice
		},
		saveActivity : function() { // funkcija za snimanje aktivnosti
			var name = $('#activityName').val(); // preuzimanje vrednosti upisanog u text box-u
			this.model.set("name", name); // setovanje name-a za model
			this.model.save({}, { // poziva se POST /api/activities, odnosno PUT /api/activities/{id}, (zavisi od toga da li je id modela null)
				success : function() { // ako je bilo 201 CREATED (za POST) ili 200 OK (za PUT)
					appRouter.navigate("activities", true); // redirekcija na rutu /#activities
				},
				error : function() {
					alert('Error');
				}
			});
		},
		close : function() {
			Backbone.Validation.unbind(this);
			$(this.el).unbind();
		}
	});
	
	UsersPageView = Backbone.View.extend({
		el : $('#container'),
		template : _.template($('#usersPageView').html()),
		initialize : function() {
			this.render();
		},
		render : function() {
			$(this.el).html(this.template());
		}
	});
	
	AppRouter = Backbone.Router.extend({ // Backbone router je zapravo kontroler unutar klijentske MVC aplikacije (tj. ove Backbone aplikacije)
		routes : { // definisanje ruta u formatu "ruta" : "funkcija". "ruta" je u browseru zapravo "#ruta"
			"" : "homeRoute",
			"activities" : "activitiesRoute",
			"activities/add" : "addActivityRoute",
			"activities/edit/:id" : "editActivityRoute",
			"users" : "usersRoute",
		},
		homeRoute : function() {
			new HomePageView();
		},
		activitiesRoute : function() {
			new ActivitiesPageView();
		},
		addActivityRoute : function() {
			if (appRouter.addEditActivityPageView) {
				appRouter.addEditActivityPageView.close();
			}
			appRouter.addEditActivityPageView = new AddEditActivityPageView();
		},
		editActivityRoute : function(id) {
			if (appRouter.addEditActivityPageView) {
				appRouter.addEditActivityPageView.close();
			}
			appRouter.addEditActivityPageView = new AddEditActivityPageView({ id : id });
		},
		usersRoute : function() {
			new UsersPageView();
		}
	});
	
	var appRouter = new AppRouter; // inicijalizacija routera da bi aplikacija radila, obavezno
	
	Backbone.history.start(); // omogućava privid navigacije (back, forward) kroz stranice preko hashtagova (#), iako je koris zapravo stalno na jednoj istoj stranici
});