package jwts.wafepa.service.impl;

import java.util.List;

import jwts.wafepa.model.Activity;
import jwts.wafepa.repository.ActivityRepository;
import jwts.wafepa.service.ActivityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Service
public class JpaActivityService implements ActivityService {

	@Autowired
	ActivityRepository activityRepository;
	
	@Override
	public Activity findOne(Long id) {
		return activityRepository.findOne(id);
	}

	@Override
	public List<Activity> findAll() {
		return activityRepository.findAll();
	}

	@Override
	public Activity save(Activity activity) {
		
		return activityRepository.save(activity);
	}

	@Override
	public void remove(Long id) throws IllegalArgumentException {
		Activity activity = findOne(id);
		if(activity==null){
			throw new IllegalArgumentException
			("Trying to remove nonexistand activity with id = " + id);
		}
		
		activityRepository.delete(activity);

	}

	@Override
	public List<Activity> findByName(String name) {
		
		return activityRepository.findByName(name);
	}

}
