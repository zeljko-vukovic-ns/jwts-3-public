<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<c:url value="/" var="root" />

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>WAFEPA</title>

        <!-- Bootstrap -->
        <link href="${root}resources/css/bootstrap.css" rel="stylesheet">
        <link href="${root}resources/css/bootstrap-theme.css" rel="stylesheet">
    </head>
    <body>
    	<%@ include file="/WEB-INF/jsp/spa/templates.jsp" %>
    	
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="${root}">Home</a>
                    <a class="navbar-brand" href="${root}#activities">Activities</a>
                    <a class="navbar-brand" href="${root}#users">Users</a>
                </div>
            </div>
        </div>
        
        <div class="jumbotron">
            <div class="container text-center">
                <h1>WAFEPA</h1>
                <p>Web Application For Evaluating Physical Activities</p>
            </div>
        </div>
        
        <div id="container" class="container">
        </div>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="${root}resources/js/jquery-2.1.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="${root}resources/js/bootstrap.js"></script>
        
        <script src="${root}resources/js/underscore.js"></script>
		<script src="${root}resources/js/backbone.js"></script>
		<script src="${root}resources/js/main.js"></script>
		
		<script type="text/javascript">
		    _.templateSettings = {
		        interpolate: /\<\@\=(.+?)\@\>/gim,
		        evaluate: /\<\@(.+?)\@\>/gim,
		        escape: /\<\@\-(.+?)\@\>/gim
		    };
		</script>
    </body>
</html>