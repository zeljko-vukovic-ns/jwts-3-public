package jwts.wafepa.service;
import jwts.wafapa.model.Activity;
import jwts.wafapa.service.ActivityService;
import jwts.wafapa.service.impl.InMemoryActivityService;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class InMemoryActivityServiceTest {
	private ActivityService activityService;
	
	@Before
	public void setUp() {
		activityService = new InMemoryActivityService();
		
		Activity swimming = new Activity();
		swimming.setName("Swimming");
		Activity running = new Activity();
		running.setName("Running");
		
		activityService.save(swimming);
		activityService.save(running);
	}
	
	@After
	public void tearDown(){
		
	}
	
	@Test
	public void testFindOne() {
	    Activity activity = activityService.findOne(1L);
	    Assert.assertNotNull(activity);
	    Assert.assertEquals("Running", activity.getName());
	}
}
