$(document).ready(function() {
	
	HomePageView = Backbone.View.extend({
		el : $('#container'),
		template : _.template($('#homePageView').html()),
		initialize : function() {
			this.render();
		},
		render : function() {
			$(this.el).html(this.template());
		}
	});
	
	ActivitiesPageView = Backbone.View.extend({
		el : $('#container'),
		template : _.template($('#activitiesPageView').html()),
		initialize : function() {
			this.render();
			this.showActivities();
		},
		render : function() {
			$(this.el).html(this.template());
		},
		showActivities : function() {
			new ActivityCollectionView({ el : $('#activitiesTable tbody') });
		}
	});
	
	ActivityModel = Backbone.Model.extend({
		urlRoot : 'api/activities',
		defaults : {
			id : null,
			name : null
		}
	});
	
	ActivityCollection = Backbone.Collection.extend({
		url : 'api/activities',
		model : ActivityModel
	});
	
	ActivityCollectionView = Backbone.View.extend({
		model : new ActivityCollection,
		initialize : function() {
			var self = this;
			this.model.fetch({
				success : function() {
					self.render();
				},
				error : function() {
					alert('Error');
				}
			});
		},
		render : function() {
			for (id in this.model.models) {
				var activity = this.model.models[id];
				var activityItemView = new ActivityItemView({ model : activity });
				var activityItemViewDOM = activityItemView.render().el;
				$(this.el).append(activityItemViewDOM);
			}
		}
	});
	
	ActivityItemView = Backbone.View.extend({
		tagName : "tr",
		template : _.template($('#activityItemView').html()),
		events : {
			"click .removeActivity" : "removeActivity"
		},
		render : function() {
			$(this.el).html(this.template({ activity : this.model.toJSON() }));
			return this;
		},
		removeActivity : function() {
			this.model.destroy();
			$(this.el).remove();
			return false;
		}
	});
	
	UsersPageView = Backbone.View.extend({
		el : $('#container'),
		template : _.template($('#usersPageView').html()),
		initialize : function() {
			this.render();
		},
		render : function() {
			$(this.el).html(this.template());
		}
	});
	
	AppRouter = Backbone.Router.extend({
		routes : {
			"" : "homeRoute",
			"activities" : "activitiesRoute",
			"users" : "usersRoute"
		},
		homeRoute : function() {
			new HomePageView();
		},
		activitiesRoute : function() {
			new ActivitiesPageView();
		},
		usersRoute : function() {
			new UsersPageView();
		}
	});
	
	var appRouter = new AppRouter;
	
	Backbone.history.start();
});